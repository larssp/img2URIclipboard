﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Text.RegularExpressions;

namespace img2URIclipboard
{
    class Program
    {
        [STAThread]
        static void Main(string[] args)
        {
            Int32 test = args.Length;
            // Console.WriteLine("num of args: {0}",test);
            if (test>0)
            {
                string filepath = args.ElementAt(0);
                Match m = Regex.Match(filepath, "[^.]+$");
                string filefmt = "";
                while (m.Success) 
                {
                    //Console.WriteLine("'{0}' found at position {1}", m.Value, m.Index);
                    filefmt = m.Value;
                    m = m.NextMatch();
                }   
                byte[] buffer = ReadFile(filepath);
                string temp_inBase64 = Convert.ToBase64String(buffer);
                string urihtmlstr = "<img src=\"data:image/"+filefmt+";base64,"+temp_inBase64+"\">";
                // png;base64,",temp_inBase64
                //Console.WriteLine("urihtmlstr: {0}", urihtmlstr);
                Clipboard.SetText(urihtmlstr);
            }
            else
            {
                // nichts
            }

        }
        public static byte[] ReadFile(string filePath)
        {
            byte[] buffer;
            FileStream fileStream = new FileStream(filePath, FileMode.Open, FileAccess.Read);
            try
            {
                int length = (int)fileStream.Length;  // get file length
                buffer = new byte[length];            // create buffer
                int count;                            // actual number of bytes read
                int sum = 0;                          // total number of bytes read

                // read until Read method returns 0 (end of the stream has been reached)
                while ((count = fileStream.Read(buffer, sum, length - sum)) > 0)
                    sum += count;  // sum is a buffer offset for next reading
            }
            finally
            {
                fileStream.Close();
            }
            return buffer;
        }

    }
}
