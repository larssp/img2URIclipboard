The [URI](https://en.wikipedia.org/wiki/Data_URI_scheme) of a file with html tags is copied to the clipboard.

usage:

    img2URIclipboard.exe PathToFile

result:  

    <img src="data:image/png;base64,iVBORw0K...">

application:
configure [greenshot](http://getgreenshot.org) to use this program.
this is a fast way to include screenshots in [jupyter](http://jupyter.org) notebooks.